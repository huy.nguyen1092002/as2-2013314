using ManageCustomer.Models;
using Microsoft.AspNetCore.Mvc;

namespace ManageCustomer.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class BranchesController : ControllerBase
    {
        public static List<Branch> branches = new List<Branch>();
        public BranchesController()
        {

        }
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(branches);
        }
        [HttpPost]
        public IActionResult Add(Branch new_branch)
        {
            var branch = new Branch
            {
                BranchId = new_branch.BranchId,
                Name = new_branch.Name,
                Address = new_branch.Address,
                City = new_branch.City,
                State = new_branch.State,
                ZipCode = new_branch.ZipCode
            };
            branches.Add(branch);
            return Ok(new
            {
                Success = true,
                Data = branch
            });
        }
        [HttpGet("{id}")]
        public IActionResult Detail(int id)
        {
            try
            {
                var branch = branches.SingleOrDefault(cn => cn.BranchId == id);
                if (branch == null)
                {
                    return NotFound();
                }
                return Ok(branch);
            }
            catch (System.Exception)
            {
                return BadRequest();
            }

        }
        [HttpPut("{id}")]
        public IActionResult Edit(int id, Branch edit_branch)
        {
            try
            {
                var branch = branches.SingleOrDefault(cn => cn.BranchId == id);
                if (branch == null)
                {
                    return NotFound();
                }
                // Update branch
                if (id != edit_branch.BranchId)
                {
                    return BadRequest();
                }
                branch.Name = edit_branch.Name;
                branch.Address = edit_branch.Address;
                branch.City = edit_branch.City;
                branch.State = edit_branch.State;
                branch.ZipCode = edit_branch.ZipCode;
                return Ok();
            }
            catch (System.Exception)
            {
                return BadRequest();
            }
        }
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                var branch = branches.SingleOrDefault(cn => cn.BranchId == id);
                if (branch == null)
                {
                    return NotFound();
                }
                branches.Remove(branch);
                return Ok();
            }
            catch (System.Exception)
            {
                return BadRequest();
            }
        }
    }
}